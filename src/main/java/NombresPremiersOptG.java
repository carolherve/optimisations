/**
 * Created by nico on 2016-11-15.
 *
 * Early casting of sqrt
 */
public class NombresPremiersOptG extends Thread
{
    int MAX;
    boolean[] listePremiers;

    public NombresPremiersOptG(int MAX)
    {
        this.MAX = MAX;
        listePremiers = new boolean[this.MAX];
    }

    public void run()
    {
        long tempsInitListe = System.currentTimeMillis();
        initListe();
        tempsInitListe = System.currentTimeMillis() - tempsInitListe;

        long temps = calculePremiers();

        long tempsComptePremiers = System.currentTimeMillis();
        int total = comptePremiers();
        tempsComptePremiers = System.currentTimeMillis() - tempsComptePremiers;

        System.out.println(getClass().toString() + " " + total + " nombres premiers trouvés en " + temps + " ms");
        System.out.println(getClass().toString() + " temps init   : " + tempsInitListe + " ms");
        System.out.println(getClass().toString() + " temps compte : " + tempsComptePremiers + " ms\n");
    }

    private void initListe()
    {
        for (int i=0; i<this.MAX; i++) {
            listePremiers[i] = false;
        }
    }

    private int comptePremiers()
    {
        int total = 0;
        for (int i=0; i<this.MAX; i++) {
            if (true == listePremiers[i]) {
                total++;
            }
        }
        return total;
    }

    private boolean estPremier(int candidat)
    {
        boolean premier = true;
        int maxDiviseur = (int)Math.sqrt(candidat);
        for (int diviseur = 3; diviseur <= maxDiviseur; diviseur += 2) {
            int resultat = candidat % diviseur;
            if (0 == resultat) {
                premier = false;
                break;
            }
        }
        return premier;
    }

    private long calculePremiers()
    {
        long debut = System.currentTimeMillis();
        for (int candidat=3; candidat < MAX; candidat += 2) {
            if (estPremier(candidat)) {
                listePremiers[candidat] = true;
            }
        }
        return (System.currentTimeMillis() - debut);
    }

}
