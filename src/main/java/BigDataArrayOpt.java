/**
 * Created by nico on 2016-11-15.
 */
public class BigDataArrayOpt extends BigDataArrayRef
{
    public long calculOpt3(int hauteur)
    {
        long start = System.currentTimeMillis();

        // OPT-3
        double coeff = Math.PI * Math.cos(hauteur);
        for (int i = 0; i < data.length; i++) {
            int tmp = this.data[i] *= coeff;
        }

        long end = System.currentTimeMillis();
        return (end - start);
    }
}
