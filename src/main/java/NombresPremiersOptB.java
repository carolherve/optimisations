import java.util.ArrayList;
import java.util.List;

/**
 * Created by nico on 2016-11-15.
 *
 * Remplacé ArrayList par un tableau de booléens
 */
public class NombresPremiersOptB extends Thread
{
    int MAX;
    boolean[] listePremiers;

    public NombresPremiersOptB(int MAX)
    {
        this.MAX = MAX;
        listePremiers = new boolean[this.MAX];
    }

    public void run()
    {
        long tempsInitListe = System.currentTimeMillis();
        initListe();
        tempsInitListe = System.currentTimeMillis() - tempsInitListe;

        long temps = calculePremiers();

        long tempsComptePremiers = System.currentTimeMillis();
        int total = comptePremiers();
        tempsComptePremiers = System.currentTimeMillis() - tempsComptePremiers;

        System.out.println(getClass().toString() + " " + total + " nombres premiers trouvés en " + temps + " ms");
        System.out.println(getClass().toString() + " temps init   : " + tempsInitListe + " ms");
        System.out.println(getClass().toString() + " temps compte : " + tempsComptePremiers + " ms\n");
    }

    private void initListe()
    {
        for (int i=0; i<this.MAX; i++) {
            listePremiers[i] = false;
        }
    }

    private int comptePremiers()
    {
        int total = 0;
        for (int i=0; i<this.MAX; i++) {
            if (true == listePremiers[i]) {
                total++;
            }
        }
        return total;
    }

    private boolean estPremier(int candidat)
    {
        boolean premier = true;
        for (int diviseur = 2; diviseur < candidat; diviseur++) {
            int resultat = candidat % diviseur;
            if (0 == resultat) {
                premier = false;
                break;
            }
        }
        return premier;
    }

    private long calculePremiers()
    {
        long debut = System.currentTimeMillis();
        for (int candidat=3; candidat < MAX; candidat++) {
            if (estPremier(candidat)) {
                listePremiers[candidat] = true;
            }
        }
        return (System.currentTimeMillis() - debut);
    }

}
