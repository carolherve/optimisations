import java.util.Scanner;

/**
 * Created by nico on 2016-11-15.
 */
public class NombresPremiers
{
    public static void main(String[] args)
    {
        int MAX = 3000000;

        NombresPremiersREF ref = new NombresPremiersREF(MAX);
        NombresPremiersOptA optA = new NombresPremiersOptA(MAX);
        NombresPremiersOptB optB = new NombresPremiersOptB(MAX);
        NombresPremiersOptC optC = new NombresPremiersOptC(MAX);
        NombresPremiersOptD optD = new NombresPremiersOptD(MAX);
        NombresPremiersOptE optE = new NombresPremiersOptE(MAX);
        NombresPremiersOptF optF = new NombresPremiersOptF(MAX);
        NombresPremiersOptG optG = new NombresPremiersOptG(MAX);

        /*
        System.out.println("Press [enter] to continue");
        Scanner scan = new Scanner(System.in);
        scan.nextLine();
        System.out.println("Starting!");
         */
        
        //ref.run();
        //optA.run();
        //optB.run();
        //optC.run();
        //optD.run();        
        optE.run();
        optF.run();
        optG.run();
    }
}
