import java.util.Scanner;

/**
 * Created by nico on 2016-11-15.
 */
public class LoopExample
{
    static int innerLoop = 30;
    static int outterLoop = 5000000;

    public static void main(String[] args)
    {
    	System.out.println("Press [enter] to continue");
        Scanner scan = new Scanner(System.in);
        scan.nextLine();
        System.out.println("Starting!\n");
    	
        performanceB();
        System.out.println();
        performanceA();
    }

    public static void performanceA()
    {
        long start = System.currentTimeMillis();
        double x = 1;
        for (int j=0; j<outterLoop; j++) {
            x = 1;
            for (int i = 0; i < innerLoop; i++) {
                x = multA(x);
            }
        }
        long temps = System.currentTimeMillis() - start;
        System.out.println("A : temps total : " + temps);
        System.out.println("A : valeur de x : " + x);
    }

    public static int multB(int x)
    {
    	return x * 2;
    }
    
    public static double multA(double x)
    {
    	return x * 2.0;
    }

    public static void performanceB()
    {
        long start = System.currentTimeMillis();
        int x = 1;
        for (int j=0; j<outterLoop; j++) {
            x = 1;
            for (int i = 0; i < innerLoop; i++) {
                x = multB(x);
            }
        }
        long temps = System.currentTimeMillis() - start;
        System.out.println("B : temps total : " + temps);
        System.out.println("B : valeur de x : " + x);
    }

}
